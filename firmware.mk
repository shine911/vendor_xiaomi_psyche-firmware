PRODUCT_COPY_FILES += \
        vendor/xiaomi-firmware/psyche/firmware-update/abl.elf:install/firmware-update/abl.elf \
        vendor/xiaomi-firmware/psyche/firmware-update/aop.mbn:install/firmware-update/aop.mbn \
        vendor/xiaomi-firmware/psyche/firmware-update/BTFM.bin:install/firmware-update/BTFM.bin \
        vendor/xiaomi-firmware/psyche/firmware-update/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
        vendor/xiaomi-firmware/psyche/firmware-update/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
        vendor/xiaomi-firmware/psyche/firmware-update/devcfg.mbn:install/firmware-update/devcfg.mbn \
        vendor/xiaomi-firmware/psyche/firmware-update/dspso.bin:install/firmware-update/dspso.bin \
        vendor/xiaomi-firmware/psyche/firmware-update/featenabler.mbn:install/firmware-update/featenabler.mbn \
        vendor/xiaomi-firmware/psyche/firmware-update/hyp.mbn:install/firmware-update/hyp.mbn \
        vendor/xiaomi-firmware/psyche/firmware-update/km41.mbn:install/firmware-update/km41.mbn \
        vendor/xiaomi-firmware/psyche/firmware-update/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
        vendor/xiaomi-firmware/psyche/firmware-update/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
        vendor/xiaomi-firmware/psyche/firmware-update/storsec.mbn:install/firmware-update/storsec.mbn \
        vendor/xiaomi-firmware/psyche/firmware-update/tz.mbn:install/firmware-update/tz.mbn \
        vendor/xiaomi-firmware/psyche/firmware-update/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
        vendor/xiaomi-firmware/psyche/firmware-update/xbl.elf:install/firmware-update/xbl.elf \
        vendor/xiaomi-firmware/psyche/firmware-update/xbl_config.elf:install/firmware-update/xbl_config.elf \
